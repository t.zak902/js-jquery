
//It is also called Function Expression

//Example:

var area = function(width,height){
    return width * height;
};

var size = area(5,5);
var el = document.getElementById('size');
el.textContent = size;