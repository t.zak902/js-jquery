// This file is linked to MultipleValues_function.html

function getSize(width,height,depth){
    var area = width * height;
    var volume = width * height * depth;
    var sizes = [area, volume];
    return sizes;
}

var areaOne = getSize(4,3,5)[0];
var el = document.getElementById('areaOne');
el.textContent = areaOne;

var volumeOne = getSize(3,2,3)[1];
var el = document.getElementById('volumeOne');
el.textContent = volumeOne;