// area is local variable
// also called : Local (Or Function-level) Scope

function getArea(width, height){
    var area = width * height;  
    return area;
}

// wallSize is global variable
// also called : Global Scope
var wallSize = getArea (3,2); 
document.write(wallSize);