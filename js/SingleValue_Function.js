// This file is linked to SingleValue_function.html


//I'm initializing the function
//I'm defining calculation to be based on 2 params, first is width, second is height
//Then I'm returning the value or the result
function calculateArea(width, height){
    var area = width * height;
    return area;
}
//I'm creating a new calculations and saving the result in a variable
var WallOne = calculateArea(5, 6);
var el = document.getElementById('WallOne');
el.textContent = WallOne;


var WallTwo = calculateArea(4, 3);
var el = document.getElementById('WallTwo');
el.textContent = WallTwo;