//Called IMMEDIATELY INVOKED FUNCTION EXPRESSIONS
//Pronounced 'iffy'

//Created to prevent functions conflicting with 
//other specially when used with multiple scripts

/**
 * When to use anonymous functions and IIFES:
 * They are used for code that only needs to run once within a task
 * rather than repeatedly being called by other parts of the script
 * Example:
 * As an argument when a function is called (to calculate a value for that function)
 * To assign the value of a property to an object
 * In event handlers and listeners to perform a task when an event occurs
 * To prevent conflicts between two scripts that might use the same variable names.
 */

var area = (function () {
    var width = 3;
    var height = 5;
    return width * height;
} () );

var el = document.getElementById('area');
el.textContent = area;