var hotel = {
    name : 'Hotel Moulana',
    rooms : '600',
    booked : '500',
    gym : true,
    roomTypes: ['twin','double','suite'],

    checkAvailability: function(){
        return this.rooms - this.booked;
    }

};

var hotelName = hotel['name'];
var el = document.getElementById('hotelName');
el.textContent = hotelName;


var roomsFree = hotel['checkAvailability']();
var el = document.getElementById('roomsFree');
el.textContent = roomsFree;
