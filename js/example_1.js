/**This file is linked to example_1.html */

//Example instructions:
//A Variable called name holds the text the sign will show
//A property called length is used to determine how many characters are in the string
//The cost of the name is calculated by multiplying the number of tiles by the cost of each one
//The grand total is created by adding $7 for shipping

var name = "Howdy Molly";
var elName = document.getElementById('name');
elName.textContent = name;

var custom_sign = "zakaria";
var elName = document.getElementById('custom_sign');
elName.textContent = custom_sign;

var tiles = custom_sign.length;
var elName = document.getElementById('tiles');
elName.textContent = tiles;

var sign_cost_each = 5;

var subtotal = sign_cost_each * tiles;
var elName = document.getElementById('subtotal');
elName.textContent = subtotal;

var shipping = 7;
var elName = document.getElementById('shipping');
elName.textContent = shipping;

var grand_total = subtotal + shipping;
var elName = document.getElementById('grand_total');
elName.textContent = grand_total;

